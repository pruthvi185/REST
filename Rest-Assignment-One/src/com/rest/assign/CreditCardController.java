package com.rest.assign;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/creditcard")  
public class CreditCardController {
	
	@POST  
    @Path("/check")  
    public Response addUser(  
        @FormParam("cardNumber") int id 
       ) {  
   
        return Response.status(200)  
            .entity(id%2 == 0)  
            .build();  
    }  
}
