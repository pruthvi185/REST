package movies;

import java.util.Arrays;
import java.util.List;

import javax.websocket.server.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/movies")

public class Display {
	List<Movie>m;
	
	public Display()
	{

    	Movie m1=new Movie();
    	m1.setMovieid("MI");
    	m1.setMoviename("mission Impossible");
    	m1.setMovieactor("Tom Cruise");
    	m1.setCollection(12);
		
    	Movie m2=new Movie();
    	m2.setMovieid("Marvel");
    	m2.setMoviename("Avengers");
    	m2.setMovieactor("Tony Stark");
    	m2.setCollection(13);
    	
    	Movie m3=new Movie();
    	m3.setMovieid("HP");
    	m3.setMoviename("Harry Potter");
    	m3.setMovieactor("Daniel Radcliffe");
    	m3.setCollection(14);
		
    	Movie m4=new Movie();
    	m4.setMovieid("KP");
    	m4.setMoviename("Kungfu Panda");
    	m4.setMovieactor("Panda");
    	m4.setCollection(10);
    	
    	Movie m5=new Movie();
    	m5.setMovieid("DK");
    	m5.setMoviename("Dark Knight");
    	m5.setMovieactor("Christian Bale");
    	m5.setCollection(16);
    	
    	Movie m6=new Movie();
    	m6.setMovieid("FnF");
    	m6.setMoviename("Fast And Furious");
    	m6.setMovieactor("Vin Diesel");
    	m6.setCollection(18);
		
    	Movie m7=new Movie();
    	m7.setMovieid("PR");
    	m7.setMoviename("Pacific Rim");
    	m7.setMovieactor("Charlie Hunnam");
    	m7.setCollection(10);
   	 m= Arrays.asList(m1,m2,m3,m4,m5,m6,m7);

    
	}
	
 

	@GET
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML,MediaType.TEXT_XML})
	public List<Movie> showdetails(){
   	
   
		return m;
	}
	@GET
	@Path("/find")
	@Produces(MediaType.APPLICATION_XML)
	public Movie showdetails1(@QueryParam("id") String id){
   	    
		for(Movie mx: m)
		{
			
			
			if(mx.getMovieid().equals(id))
			{
				 
				return mx;
			}
		}
		return null;
   
	}

	
}
