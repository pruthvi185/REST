package movies;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Movie {

	private String movieid;
	
	private String moviename;
	private String movieactor;
	private float collection;
	public Movie(){}
	public Movie(String movieid, String moviename, String movieactor, float collection) {
		super();
		this.movieid = movieid;
		this.moviename = moviename;
		this.movieactor = movieactor;
		this.collection = collection;
	}
	public String getMovieid() {
		return movieid;
	}
	public void setMovieid(String movieid) {
		this.movieid = movieid;
	}
	public String getMoviename() {
		return moviename;
	}
	public void setMoviename(String moviename) {
		this.moviename = moviename;
	}
	public String getMovieactor() {
		return movieactor;
	}
	public void setMovieactor(String movieactor) {
		this.movieactor = movieactor;
	}
	public float getCollection() {
		return collection;
	}
	public void setCollection(float collection) {
		this.collection = collection;
	}
	
	
	
	
	
	
	
}
