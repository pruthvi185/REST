package Album;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/albums")
public class Display {

	List<Album> a;

	public Display(){
		
		Album a1 =new Album();
		
		a1.setTitle("Shape Of You");
		a1.setSinger("Ed Sheeran");
		
		
       Album a2 =new Album();
		
		a2.setTitle("Love me like you do");
		a2.setSinger("Adele");
		
		 a= Arrays.asList(a1,a2);
	}
	

	@GET
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public List<Album> showdetails(){
   	
   
		return a;
	}
	
	
	
}
